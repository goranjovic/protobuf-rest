#!/bin/bash

mkdir -p target/gen-java-src
protoc -I=/usr/include -I=/usr/local/include -I=resources/proto \
       --java_out=target/gen-java-src resources/proto/*.proto