# protobuf-rest

FIXME

## Prerequisites

- Java 8
- Leiningen 2.0.0 or above installed.
- Docker 18.06
- Docker Compose 1.8.0

## Building

To build the docker image:

`lein build`

This will generate the protobuf Java source files, compile all sources, build an
uberjar file and include it in the overall docker image.

## Running

To both rebuild the project and run it:

`lein start`

or to run a prebuilt version:

`docker-compose up`

This will start a rest service, ready to accept incoming post requests at:

`http://localhost:9081/import`

e.g.

```
curl -X POST \
  http://localhost:9081/import \
  -H 'content-type: application/json' \
  -d '{"name" : "Chuck", "id": 42}'
```

## Testing 

To run the tests:

`lein test`

## License

Copyright © 2019 FIXME
