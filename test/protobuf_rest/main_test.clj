(ns protobuf-rest.main-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [protobuf-rest.main :as main]
            [protobuf-rest.config :as config]
            [clojure.string :as string]
            [clojure.java.io :as io])
  (:import (java.util Date)
           (java.text SimpleDateFormat)))

(defn format-date [date]
  (.format (new SimpleDateFormat "yyyy_MM_dd") date))

(defn today-str []
  (-> (new Date)
      format-date))

(deftest test-app

  (config/setenv! :test)

  (testing "main route"
    (let [response (main/app-routes (mock/request :get "/"))]
      (is (= (:status response) 200))
      (is (= (:body response) main/usage))))

  (testing "not-found route"
    (let [response (main/app-routes (mock/request :get "/invalid"))]
      (is (= (:status response) 404))
      (is (= (:body response) main/not-found))))

  (testing "service"
    (let [filepath-template (config/property :output-filepath)
          filepath-actual (string/replace filepath-template "yyyy_mm_dd" (today-str))
          response (main/app-routes (-> (mock/request :post "/import")
                                        (mock/json-body {:id 1 :name "test"})))]
      (is (= (:status response) 200))
      (is (.exists (io/as-file filepath-actual))))))
