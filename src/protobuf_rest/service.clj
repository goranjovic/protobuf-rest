(ns protobuf-rest.service
  (:import example.Example$Item
           org.eclipse.jetty.util.RolloverFileOutputStream)
  (:require [compojure.core :refer :all]
            [clojure.tools.logging :as log]
            [protobuf.core :as protobuf]
            [protobuf-rest.config :as config]))

(defn create-item [data]
  (protobuf/create Example$Item data))

(defn process-item [data]
  (log/info "Saving " data)
  (->> data
       create-item
       protobuf/->bytes
       (spit (new RolloverFileOutputStream (config/property :output-filepath) true 1000))))