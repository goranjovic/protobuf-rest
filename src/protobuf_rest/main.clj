(ns protobuf-rest.main
  (:require [compojure.core :refer :all]
            [clojure.tools.logging :as log]
            [compojure.route :as route]
            [ring.util.response :as r]
            [clojure.data.json :as json]
            [ring.adapter.jetty :as jetty]
            [protobuf-rest.service :as service]
            [protobuf-rest.config :as config])
  (:gen-class))

(defn parse-data [data]
  (-> data
      (select-keys ["id" "name"])))

(defn import-handler [request]
  (let [payload (-> request :body slurp json/read-str parse-data)]
    (service/process-item payload)
    (r/response "OK")))

(def usage
  "To use the rest service, POST a json body to: http:/localhost:9081/import")

(def not-found
  "Not Found")

(defroutes app-routes
  (GET "/" [] usage)
  (POST "/import" [] import-handler)
  (route/not-found not-found))

(defn -main [& args]
  (let [mode (keyword (first args))]
    (log/info "Starting server in " mode " mode")
    (config/setenv! mode)
    (jetty/run-jetty app-routes {:port  9081
                                 :join? false})))
