(ns protobuf-rest.config)

(def env
  (atom nil))

(defn setenv! [mode]
  (reset! env mode))

(def properties-map
  {:dev  {:output-filepath "file-yyyy_mm_dd.out"}
   :prod {:output-filepath "/data/file-yyyy_mm_dd.out"}
   :test {:output-filepath "tmp/file-yyyy_mm_dd.out"}})

(defn property [key]
  (get-in properties-map [@env key]))
