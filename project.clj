(defproject protobuf-rest "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [compojure "1.4.0"]
                 [ring/ring-jetty-adapter "1.4.0"]
                 [ring/ring-defaults "0.3.2"]
                 [org.clojure/data.json "0.2.6"]
                 [log4j/log4j "1.2.17"]
                 [org.clojure/tools.logging "0.2.6"]
                 [clojusc/protobuf "3.5.1-v1.1"]]
  :aliases {"protobuf" ["shell" "./scripts/compile-protobuf.sh"]
            "build"    ["do"
                        ["clean"]
                        ["protobuf"]
                        ["uberjar"]
                        ["shell" "./scripts/docker-build.sh"]]
            "start"    ["do"
                        ["build"]
                        ["shell" "./scripts/docker-compose-run.sh"]]}
  :source-paths ["src"]
  :java-source-paths ["target/gen-java-src"]
  :main protobuf-rest.main
  :profiles {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                                  [ring/ring-mock "0.3.2"]]
                   :plugins      [[lein-shell "0.5.0"]]}
             :uberjar {:aot          :all
                       :auto-clean   false
                       :uberjar-name "protobuf-rest.jar"}})
